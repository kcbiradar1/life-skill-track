**Good Practice for Software Development**

1. Points that are new to me.
   - Taking notes while discussion going on and conforming with explainer.
   - Having good communication with team members.

2. Which area do you think need to improve on? What are your ideas to make progress in that area?
   - Taking proper notes while discussion is happening.
   - Having good communication skills.
   - Understanding importance of both of these things in work area.
   - I'm focusing on these things to improve on them.
## Prevention of Sexual Harassment

#### Q. What kinds of behaviour cause sexaul harassment?
- Verbal Harassment
  - Spreading rumors about a person's personal or sexual life
  - Comments about clothing
  - A person's body
- Visual Harassment
  - Drawings/Pictures
  - Screensavers
  - Obscence posters
- Physical Harassment
  - Sexual assault
  - Impeding or blocking movement
  - Inappropriate touching

#### Q. What would you do in case you face or witness any incident or repeated incidents of such behaviour?
- Inform the higher authority or file a complaint with the police.

#### Q. Explain different scenarios enacted by actors.
Different scenarios involving harassment
  - Workplace Harassment
  - Online Harassment
  - Public Harassment

#### Q. How to handle cases of harassment?
- Reporting incidents.
- Ensure Safety.
- Seeking help/support.
- Knowing own rights/Following employer's polices and procedures.

#### Q. How to behave appropriately?
- Treat everyone with respect.
- Use polite language.

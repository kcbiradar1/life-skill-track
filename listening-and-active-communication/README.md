## Listening and Active Communication

#### 1. What are the steps/strategies to do Active Listening?
- Fully hearing and understanding the meaning what someone saying.
- Avoid getting distracted by own thoughts.
- Focusing on topic and speaker.
- Not to interrupt speaker, while he/she explaining something.
- Showing interest towards topic with body language.
- Taking notes during important conversations.

#### 2. According Fisher's model, what are the key points of Reflective Listening?
- Understanding speaker's idea.
- confirming idea has been understood correctly.
- Taking notes in Technical discussion so not to miss any of the minute details.
  
#### 3. What are the obstacles in your listening process?
- Distracting by own thoughts.
- Not interested in the speaker's topic.
- using social media

#### 4. What can you do to improve your listening?
- Paying attention towards the speaker.
- Showing interest towards the topics, that are speaker explaining.
- Taking notes of important points while listening.
- Active involvement in the conversations.

#### 5. When do you switch to Passive communication style in your day to day life?
- When my personal opinions do not affect the final outcome of the process, I will switch to Passive communication.

#### 6. When do you switch to Aggressive communication styles in your day to day life?
- When I feel extremely frustrated, I switch to Aggressive communication.

#### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- When I am with my close friends, I tend to switch to Passive Aggressive communication.

#### 8. How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?
- Respecting the feelings and needs of others.
- Reducing the arguments while having conversations.
- Having good body language and tone.
- Understanding limitations.
# OSI MODEL

The Open Systems Interconnection (OSI) model describes seven layers that computer systems use to communicate over a network.

<p align="center">
  <img src="https://sepiocyber.com/wp-content/uploads/2023/10/Physical-Layer-of-the-OSI-Model.png" />
</p>


## Application Layer

- An application layer is an abstraction layer that specifies the shared communication protocols and interface methods used by hosts in a communications network.

- The application layer is used by end-user software such as web browsers and email clients.
  
- It provides protocols that allow software to send and receive information and present meaningful data to users.
  
- A few examples of application layer protocols are the Hypertext Transfer Protocol (HTTP), File Transfer Protocol (FTP), Post Office Protocol (POP), Simple Mail Transfer Protocol (SMTP), and Domain Name System (DNS).

## Presentation Layer

- The presentation layer prepares data for the application layer.
  
- It defines how two devices should encode, encrypt, and compress data so it is received correctly on the other end.
  
- The presentation layer takes any data transmitted by the application layer and prepares it for transmission over the session layer.
  
- The presentation layer ensures the information that the application layer of one system sends out is readable by the application layer of another system.

- The presentation layer might be able to translate between multiple data formats using a common format.


## Session Layer

- The session layer creates communication channels, called sessions, between devices.
  
- It is responsible for opening sessions, ensuring they remain open and functional while data is being transferred, and closing them when communication ends.
  
- The session layer can also set checkpoints during a data transfer if the session is interrupted, devices can resume data transfer from the last checkpoint.
  
## Transport Layer

- The transport layer takes data transferred in the session layer and breaks it into “segments” on the transmitting end.
  
- It is responsible for reassembling the segments on the receiving end, turning it back into data that can be used by the session layer.
  
- The transport layer carries out flow control, sending data at a rate that matches the connection speed of the receiving device, and error control, checking if data was received incorrectly and if not, requesting it again.

## Network Layer

- The network layer provides the means of transferring variable-length network packets from a source to a destination host via one or more networks.

- The network layer has two main functions. One is breaking up segments into network packets, and reassembling the packets on the receiving end.
  
- The other is routing packets by discovering the best path across a physical network.
  
- The network layer uses network addresses to route packets to a destination node.

## Data Link Layer

- The data link layer establishes and terminates a connection between two physically-connected nodes on a network.
  
- t breaks up packets into frames and sends them from source to destination.
  
- This layer is composed of two parts—Logical Link Control which identifies network protocols, performs error checking and synchronizes frames, and Media Access Control (MAC) which uses MAC addresses to connect devices and define permissions to transmit and receive data.

## Physical Layer

- The physical layer is responsible for the physical cable or wireless connection between network nodes.

- The physical layer defines the means of transmitting a stream of raw bits over a physical data link connecting network nodes.

- It defines the connector, the electrical cable or wireless technology connecting the devices, and is responsible for transmission of the raw data, which is simply a series of 0s and 1s, while taking care of bit rate control.


## Adavantages of OSI Model

- The OSI model helps users and operators of computer networks.

- Determine the required hardware and software to build their network.

- Understand and communicate the process followed by components communicating across a network.
  
- Perform troubleshooting, by identifying which network layer is causing an issue and focusing efforts on that layer.Perform troubleshooting, by identifying which network layer is causing an issue and focusing efforts on that layer.
  
- The OSI model helps network device manufactures and networking software vendors.
  
- Create devices and software that can communicate with products from any other vendor, allowing open interoperability
  
- Define which parts of the network their products should work with.

- Communicate to users at which network layers their product operates – for example, only at the application layer, or across the stack.

#### Referances:

- [Blog about OSI Model](https://www.imperva.com/learn/application-security/osi-model/)
  
- [Video Referance on OSI Model](https://www.youtube.com/watch?v=vv4y_uOneC0)

- [Wiki on OSI model](https://en.wikipedia.org/wiki/OSI_model)


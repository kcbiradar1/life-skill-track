**Focus Management**

1. What is Deep Work?
   - No distractions.
   - Complete focus on work.
2. According to author how to do deep work properly, in a few points?
   - Maintaining proper sleep.
   - Staying away from distractions & reducing focus on them.
   - Developing deep work habit, and imporving day by day.
3. How can you implement the principles in your day to day life?
   - Maintain good sleep routine.
   - Practising deep work habit daily.
4. What are the dangers of social media, in brief?
   - It is distractive, while focusing on particular work.
   - Getting addictive.
   - Anxiety-related problems.
# Learning Process

### 1. what is the Feynman Technique? Explain in 1 Line.

- Feynman Teachnique explains, while learning something don't fool yourself, explain the concept using simple words and practice. If we found problem in understanding any concept go back to the sources to review. If we found anything complex while learning try to simply them to simple/understding words.

### 2. Learning How to Learn TED talk by Barbara Oakley.

- This video shares an interesting story about how Salvador Dali and Thomas Edison shifted from a relaxed state to a focused one. Both used a similar method: they would sit in a chair and relax, holding an object in their hand. When the object fell, they would start working.

### 3. What are active and diffused modes of thinking?

- Active mode means we are always paying close attention to something.
- Diffuse mode is when our brain is relaxed and not focused on anything specific.
  
### 4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.

- Deconstruct the skill
  - Breaking down the skill to smaller pieces based on what we want to be after completion of learning.

- Learn enough to self-correct.

- Remove practice barriers.

- Practice at least 20 hours.

### 5. What are some of the actions you can take going forward to improve your learning process?

- Preserving attetion while learning the concepts.
- Removing practice barriers.
- Practice enough to get hold on particular topic.
- Make the process of learning fun and easy.
- Understanding concepts in own words.
## Grit and Growth Mindset

1. Paraphrase the video in a few lines. Use your own words
   - grit is passion and perseverance for very long-term goals.
   - The talent doesn't make you gritty.
   - growth mindset is very important to build a grit.
   - Ability to learn is not fixed, that it can chage with effort
2. Paraphrase the video in a few lines in your own words.
   - Growth mindset is that skills and intelligence are grown and developed. People who are good at something are good because they built that ability and people who aren't-are not good because they haven't done the work.
3. What is the Internal Locus of Control? What is the key point in the video?
   - The degree to which you believe you have control over your life.
   - Internal locus of control : Effort that allow to do well(HardWork)
   - Internal locus of control is a key to staying motivated.
4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).
   - Self belief.
   - Question your own assumptions.
   - Develop own life curriculum.
   - Honor the struggle.
5. What are your ideas to take action and build Growth Mindset?
   - I know more efforts lead to better understanding.
   - I will understand each concept properly.
   - I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.
   - will understand the users very well. I will serve them and the society by writing rock solid excellent software.
   - I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
1. What are the activities you do that make you relax - Calm quadrant?
   - Activities that make me relax are
   - Participating in type-racer contest.
   - Watching movies.
   - Listening songs.
   - Playing online games.
   - Walk
2. When do you find getting into the Stress quadrant?
   - Things that are push me in Stress quadrant
   - Not completing particular work on time.
   - Not having confidance on particular topic.
   - Public speaking.
3. How do you understand if you are in the Excitement quadrant?
   - Things that makes me excited are
   - While playing online games.
   - Watching T20 cricket matches.
   - Learning any new concepts & algorithms.
   - Perfoming well in coding contests(Basically leetcode contest).
4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.
   - Sleep increases immunity power.
   - Sleep help us to concentrate on the things what we are learning.
   - Less sleep causes for ageing.
   - Sleep helps us to maintain good health.
5. What are some ideas that you can implement to sleep better?
   - Maintaining proper sleep schedule and following it daily.
   - By decreasing screen time.
   - Understanding importance of sleep in daily life.
6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
   - Make us to focus on things properly what we are working on.
   - Energy boster to do some work.
   - It helps us to remember things for long time.
   - Immidiate effect on things.
7. What are some steps you can take to exercise more?
   - Sticking to routine.
   - Knowing importance of maitaining good physical health.
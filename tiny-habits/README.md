1. what was the most interesting story or idea for you? (BJ Fogg video)
   - Adopting any new habit after completed daily routines

2. How can you use B = MAP to make making new habits easier? What are M, A and P.
   - M, A and P are three factors that helps us to achieve tiny habits. 
   - Motivation (M), which is the desire that one should carry to start the tiny tasks.
   - A refers the ability to do the task. P (Promt) which means a reminder to do the tasks. 

3. Why it is important to "Shine" or Celebrate after each successful completion of habit?
   - After each successful completion of habit helps for inner energy to do more work. And motivate to adopt new habits.

4. what was the most interesting story or idea for you? (1% Better Every Day Video)
   - Having clarity rather than motivation to do some work.
   - Having clarity on any work creates automatic motivation in person to complete that work.

5. What is the book's perspective about Identity?
   - The peak of true motivation is when a habit defines us.

6. Write about the book's perspective on how to make a habit easier to do?
    - With the four steps mentioned in the book, we can achive to make a habit easier. The four steps are 
   - Start the habit (cue).
   - Motivation to do (craving).
   - The action we perform (response).
   - And the reward is the end goal (reward).

7. Write about the book's perspective on how to make a habit harder to do?
   - by making habit obvious, making it attractive, making it easy , and final one is to making it immediatly reward.

8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
   - Doing things with concetration.
   - Understading importance of concetration while doing particular work. 

9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
   - Postponing any work instead of doing in particular time. I should understand importance of that task.
  